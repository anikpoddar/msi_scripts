import pandas as pd
import xml.etree.ElementTree as et
xml_file = "./hmdb_metabolites.xml"
xtree = et.parse(xml_file)
xroot = xtree.getroot()

metabolite_name=[]
accession_number=[]
formula=[]
mol_wt=[]

for root in xroot:
    # curating metabolite_status
    for child in root.iter('{http://www.hmdb.ca}metabolite'):
        status_one=child.find("{http://www.hmdb.ca}name").text
        metabolite_name.append(status_one)
        
    for child in root.iter('{http://www.hmdb.ca}metabolite'):
        accession_one=child.find("{http://www.hmdb.ca}accession").text
        accession_number.append(accession_one)
    
    for child in root.iter('{http://www.hmdb.ca}metabolite'):
        accession_one=child.find("{http://www.hmdb.ca}chemical_formula").text
        formula.append(accession_one)
    
    for child in root.iter('{http://www.hmdb.ca}metabolite'):
        accession_one=child.find("{http://www.hmdb.ca}monisotopic_molecular_weight").text
        mol_wt.append(accession_one)
    
temp = {'metabolite_name': metabolite_name, 'accession_number': accession_number, 'formula': formula, 'Molecular_wt': mol_wt }
final_df=pd.DataFrame.from_dict(temp)
final_df.to_csv("HMDB_metabolite_data.csv")
